<?xml version="1.0" encoding="UTF-8"?>

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">

    <modelVersion>4.0.0</modelVersion>
    <groupId>com.quisapps.jira</groupId>
    <artifactId>jss</artifactId>
    <name>Scripting Suite for Jira</name>
    <version>2.9.0</version>
    <description>JSS plug-in provides a convenient way to put custom conditions, validators and post-function into
        workflow in a form of Jython scripts. Plug-in also provides an ability to create on-edit validators, on-edit
        postfunction, and custom listeners.
    </description>

    <organization>
        <name>quisapps.com</name>
        <url>http://quisapps.com/</url>
    </organization>

    <url>http://quisapps.com/products/jss</url>

    <developers>
        <developer>
            <id>alex</id>
            <name>Alexander Shchagin</name>
            <email>shchagin@quisapps.com</email>
            <organization>quisapps.com</organization>
            <organizationUrl>http://quisapps.com</organizationUrl>
        </developer>
    </developers>

    <distributionManagement>
        <repository>
            <id>dev.quisapps.com</id>
            <name>dev.quisapps.com-releases</name>
            <url>http://dev.quisapps.com/artifactory/public</url>
        </repository>
    </distributionManagement>

    <packaging>atlassian-plugin</packaging>

    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>com.google.guava</groupId>
                <artifactId>guava</artifactId>
                <version>19.0</version>
            </dependency>
            <dependency>
                <groupId>com.atlassian.jira</groupId>
                <artifactId>jira-plugins</artifactId>
                <version>${jira.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <dependencies>
        <dependency>
            <groupId>org.python</groupId>
            <artifactId>jython-standalone</artifactId>
            <version>2.5.2</version>
        </dependency>
        <dependency>
            <groupId>com.atlassian.jira</groupId>
            <artifactId>jira-api</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.jira</groupId>
            <artifactId>jira-core</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>javax.ws.rs</groupId>
            <artifactId>jsr311-api</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>javax.xml.bind</groupId>
            <artifactId>jaxb-api</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.plugins.rest</groupId>
            <artifactId>atlassian-rest-common</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.activeobjects</groupId>
            <artifactId>activeobjects-plugin</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.jira</groupId>
            <artifactId>jira-func-tests</artifactId>
            <version>${jira.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.jira</groupId>
            <artifactId>jira-quick-edit-plugin</artifactId>
            <version>3.0.18</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.jira</groupId>
            <artifactId>jira-issue-nav-plugin</artifactId>
            <scope>provided</scope>
            <version>7.0.26</version>
        </dependency>
        <dependency>
            <groupId>com.atlassian.jira</groupId>
            <artifactId>jira-issue-nav-components</artifactId>
            <version>7.0.26</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.jira</groupId>
            <artifactId>jira-rest-plugin</artifactId>
            <scope>provided</scope>
        </dependency>
    </dependencies>

    <build>
        <resources>
            <resource>
                <directory>src/main/resources</directory>
                <excludes>
                    <exclude>jython/**</exclude>
                </excludes>
            </resource>
        </resources>
        <plugins>
            <plugin>
                <groupId>com.atlassian.maven.plugins</groupId>
                <artifactId>jira-maven-plugin</artifactId>
                <extensions>true</extensions>
                <version>${amps.version}</version>
                <configuration>
                    <jvmArgs>-XX:MaxPermSize=512M -Xmx2G</jvmArgs>
                    <productVersion>${jira.version}</productVersion>
                    <productDataVersion>${jira.version}</productDataVersion>
                    <enableQuickReload>true</enableQuickReload>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>3.8.1</version>
                <configuration>
                    <source>8</source>
                    <target>8</target>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-antrun-plugin</artifactId>
                <version>1.3</version>
                <executions>
                    <execution>
                        <phase>process-resources</phase>
                        <configuration>
                            <tasks>
                                <zip destfile="${project.build.outputDirectory}/jss_base_scripts.zip"
                                     basedir="src/main/resources/jython"
                                     update="true"/>
                            </tasks>
                        </configuration>
                        <goals>
                            <goal>run</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>

    <properties>
        <jira.version>9.0.0</jira.version>
        <amps.version>8.0.2</amps.version>
    </properties>
</project>
