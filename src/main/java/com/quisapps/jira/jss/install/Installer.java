package com.quisapps.jira.jss.install;

public interface Installer {
    
    void install();
    
    void reinstall();
    
    boolean isInstalled();
    
    boolean isJythonInstalled();
    
    boolean isBaseScriptsInstalled();
    
    boolean isQuickEditEnabled();
    
    void setQuickEditEnabled(boolean v);
    
    boolean isIssueNavEnabled();
    
    void setIssueNavEnabled(boolean b);
}