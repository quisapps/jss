package com.quisapps.jira.jss.install;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.apache.log4j.Logger;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.core.util.FileUtils;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.quisapps.jira.jss.JssUtils;
import com.quisapps.jira.jss.ao.InstallState;
import com.quisapps.jira.jss.jython.JythonUtil;

import net.java.ao.DBParam;

public class InstallerImpl implements Installer {
    private static Logger log = Logger.getLogger(InstallerImpl.class);
    
    private ActiveObjects activeObjects;
    
    public InstallerImpl(ActiveObjects activeObjects) {
        this.activeObjects = activeObjects;
    }
    
    @Override
    public void install() {
        try {
            installJython();
            installBaseScripts(true);
        } catch (InstallException e) {
            log.warn(e.getMessage(), e);
        }
        
        System.setProperty("python.home", JythonUtil.getJythonHome().getAbsolutePath());
    }
    
    @Override
    public void reinstall() {
        activeObjects.executeInTransaction(new TransactionCallback<Object>() {
            @Override
            public Object doInTransaction() {
                InstallState state = getInstallState();
                
                state.setJythonBaseScriptsInstalled(false);
                state.setJythonInstalled(false);
                
                state.save();
                
                return null;
            }
        });
        
        FileUtils.deleteDir(JythonUtil.getJythonHome());
        
        install();
    }
    
    private void installJython() throws InstallException {
        if (isJythonInstalled()) {
            log.info("Jython is already installed");
            return;
        }
        
        String jythonJarName = JythonUtil.getInstallerName();
        InputStream stream = getClass().getResourceAsStream("/" + jythonJarName);
        
        File jythonJarFile = new File(JssUtils.getJssHome(), jythonJarName);
        
        URL url;
        try {
            FileUtils.copyFile(stream, jythonJarFile, true);
            url = new URL("file:" + jythonJarFile.getAbsolutePath());
        } catch (Exception e) {
            throw new InstallException(e);
        }
        
        // Create the class loader for the application jar file
        try (JarClassLoader cl = new JarClassLoader(url)) {
            // Get the application's main class name
            String name = null;
            try {
                name = cl.getMainClassName();
            } catch (IOException e) {
                log.warn("I/O error while loading JAR file: " + e.getMessage(), e);
            }
            if (name == null) {
                throw new InstallException(
                        "Specified jar file does not contain a 'Main-Class' manifest attribute");
            }
            // Get arguments for the application
            File destination = JythonUtil.getJythonHome();
            if (destination.exists()) {
                throw new InstallException("Jython destination directory already exists");
            }
            
            String[] newArgs = { "-s", "-d", destination.getAbsolutePath(), "-t", "standard" };
            // Invoke application's main class
            try {
                cl.invokeClass(name, newArgs);
            } catch (Exception e) {
                throw new InstallException("Installation failed: " + e.getMessage(), e);
            }
        } catch (IOException e) {
            log.warn("I/O error while closing JAR file: " + e.getMessage(), e);
        }
        
        setJythonInstalled(true);
    }
    
    private void installBaseScripts(boolean overwrite) throws InstallException {
        if (isBaseScriptsInstalled()) {
            log.info("Base scripts are already installed");
            return;
        }
        
        try {
            File parent = new File(JythonUtil.getJythonPath());
            
            InputStream inStream = InstallerImpl.class.getResourceAsStream("/jss_base_scripts.zip");
            ZipInputStream zis = new ZipInputStream(inStream);
            ZipEntry entry;
            while ((entry = zis.getNextEntry()) != null) {
                String name = entry.getName();
                File file = new File(parent, name);
                
                if (entry.isDirectory()) {
                    log.info("Extracting directory: " + entry);
                    file.mkdirs();
                    continue;
                }
                
                log.info("Extracting: " + entry);
                if (file.exists() && !overwrite) {
                    log.info(entry + " exists, skipping");
                    continue;
                }
                
                FileUtils.copyFile(zis, file, overwrite);
            }
            zis.close();
            
            setBaseScriptsInstalled(true);
        } catch (Exception e) {
            throw new InstallException(e);
        }
    }
    
    @Override
    public boolean isInstalled() {
        return isJythonInstalled() && isBaseScriptsInstalled();
    }
    
    @Override
    public boolean isJythonInstalled() {
        return getInstallState().getJythonInstalled();
    }
    
    private void setJythonInstalled(boolean v) {
        InstallState state = getInstallState();
        state.setJythonInstalled(v);
        state.save();
    }
    
    @Override
    public boolean isBaseScriptsInstalled() {
        return getInstallState().getJythonBaseScriptsInstalled();
    }
    
    private void setBaseScriptsInstalled(boolean v) {
        InstallState state = getInstallState();
        state.setJythonBaseScriptsInstalled(v);
        state.save();
    }
    
    private InstallState getInstallState() {
        InstallState state = activeObjects.get(InstallState.class, 1);
        
        if (state == null) {
            log.debug("InstallState(1) not exists; creating...");
            state = activeObjects.executeInTransaction(new TransactionCallback<InstallState>() {
                @Override
                public InstallState doInTransaction() {
                    InstallState state = activeObjects.create(InstallState.class, new DBParam("ID", 1));
                    
                    InstallState state0 = activeObjects.get(InstallState.class, 0);
                    if (state0 != null) {
                        state.setIssueNavEnabled(state0.getIssueNavEnabled());
                        state.setQuickEditEnabled(state0.getQuickEditEnabled());
                        state.setJythonBaseScriptsInstalled(state0.getJythonBaseScriptsInstalled());
                        state.setJythonInstalled(state0.getJythonInstalled());
                    }
                    
                    state.save();
                    
                    return state;
                }
            });
        }
        
        return state;
    }
    
    @Override
    public boolean isQuickEditEnabled() {
        return getInstallState().getQuickEditEnabled();
    }
    
    @Override
    public void setQuickEditEnabled(final boolean v) {
        final InstallState state = getInstallState();
        activeObjects.executeInTransaction(new TransactionCallback<Object>() {
            @Override
            public Object doInTransaction() {
                state.setQuickEditEnabled(v);
                state.save();
                return null;
            }
        });
    }
    
    @Override
    public boolean isIssueNavEnabled() {
        return getInstallState().getIssueNavEnabled();
    }
    
    @Override
    public void setIssueNavEnabled(final boolean b) {
        final InstallState state = getInstallState();
        activeObjects.executeInTransaction(new TransactionCallback<Object>() {
            @Override
            public Object doInTransaction() {
                state.setIssueNavEnabled(b);
                state.save();
                return null;
            }
        });
    }
}
