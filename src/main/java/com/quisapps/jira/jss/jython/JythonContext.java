package com.quisapps.jira.jss.jython;

public class JythonContext {
    
    public static final String EVENT = "event";
    public static final String RESULT = "result";
    public static final String DESCRIPTION = "description";
    public static final String INVALID_FIELDS = "invalid_fields";
    public static final String ISSUE = "issue";
    public static final String LOG = "log";
    public static final String ORIGINALISSUE = "originalIssue";
    public static final String TRANSIENT_VARS = "transientVars";
}
