package com.quisapps.jira.jss;

import java.util.Map;

import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.web.action.issue.AbstractIssueSelectAction;
import com.quisapps.jira.jss.jython.JythonContext;

public interface ScriptingManager {
    
    JythonContext getJythonContext();
    
    @Deprecated
    void doEditValidation(AbstractIssueSelectAction action);
    
    @Deprecated
    void doEditPostfunction(AbstractIssueSelectAction action);
    
    boolean validateUpdate(ErrorCollection errors, MutableIssue issueObject);
    
    void executeUpdatePostfunction(MutableIssue issueObject);
    
    void executeScript(String script, Map<String, Object> context);
}
