package com.quisapps.jira.jss.ao;

import net.java.ao.Entity;

public interface InstallState extends Entity {
    
    boolean getJythonInstalled();
    
    void setJythonInstalled(boolean value);
    
    boolean getJythonBaseScriptsInstalled();
    
    void setJythonBaseScriptsInstalled(boolean v);
    
    boolean getQuickEditEnabled();
    
    void setQuickEditEnabled(boolean v);
    
    boolean getIssueNavEnabled();
    
    void setIssueNavEnabled(boolean b);
}
