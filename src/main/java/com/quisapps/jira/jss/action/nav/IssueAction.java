package com.quisapps.jira.jss.action.nav;

import com.atlassian.jira.avatar.AvatarService;
import com.atlassian.jira.bc.filter.SearchRequestService;
import com.atlassian.jira.bc.issue.IssueService;
import com.atlassian.jira.components.issueviewer.service.ActionUtilsService;
import com.atlassian.jira.components.issueviewer.service.SessionSearchService;
import com.atlassian.jira.components.issueviewer.service.SystemFilterService;
import com.atlassian.jira.components.query.SearcherService;
import com.atlassian.jira.components.query.util.UserSearchModeService;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.changehistory.ChangeHistoryManager;
import com.atlassian.jira.jql.util.JqlStringSupport;
import com.atlassian.jira.plugin.issuenav.service.issuetable.IssueTableService;
import com.atlassian.jira.plugin.issuenav.util.DefaultSearchUtil;
import com.atlassian.jira.plugin.issuenav.util.PreferredSearchLayoutService;
import com.atlassian.jira.plugin.keyboardshortcut.KeyboardShortcutManager;
import com.atlassian.jira.plugin.webfragment.SimpleLinkManager;
import com.atlassian.jira.rest.v2.issue.builder.BeanBuilderFactory;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.UserIssueHistoryManager;
import com.atlassian.jira.user.UserPropertyManager;
import com.atlassian.jira.user.preferences.UserPreferencesManager;
import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;
import com.atlassian.jira.web.action.issue.IssueMetadataHelper;
import com.atlassian.jira.web.action.issue.IssueNavigatorSearchResultsHelper;
import com.atlassian.jira.web.component.ModuleWebComponent;
import com.atlassian.jira.web.pagebuilder.JiraPageBuilderService;
import com.atlassian.plugin.web.WebInterfaceManager;
import com.quisapps.jira.jss.action.quickedit.IssueServiceWrapper;

@SuppressWarnings("serial")
public class IssueAction extends
        com.atlassian.jira.plugin.issuenav.IssueNavAction {
    
    public IssueAction(ApplicationProperties applicationProperties, KeyboardShortcutManager keyboardShortcutManager,
                       IssueService issueService, IssueTableService issueTableService,
                       VelocityRequestContextFactory velocityRequestContextFactory, JqlStringSupport jqlStringSupport,
                       UserPreferencesManager preferencesManager, SessionSearchService sessionSearchService,
                       PermissionManager permissionManager, SimpleLinkManager simpleLinkManager,
                       DefaultSearchUtil defaultSearchUtil, FeatureManager featureManager,
                       UserSearchModeService userSearchModeService, SearcherService searcherService,
                       SearchRequestService searchRequestService, SystemFilterService systemFilterService,
                       AvatarService avatarService, IssueNavigatorSearchResultsHelper searchResultsHelper,
                       BeanBuilderFactory beanBuilderFactory,
                       com.atlassian.jira.components.issueviewer.viewissue.webpanel.WebPanelMapperUtil webPanelMapperUtil,
                       IssueMetadataHelper issueMetadataHelper, ChangeHistoryManager changeHistoryManager,
                       ActionUtilsService actionUtilsService, WebInterfaceManager webInterfaceManager,
                       ModuleWebComponent moduleWebComponent, UserIssueHistoryManager userIssueHistoryManager,
                       PreferredSearchLayoutService preferredSearchLayoutService, UserPropertyManager userPropertyManager,
                       JiraPageBuilderService jiraPageBuilderService)
    {
        super(applicationProperties, keyboardShortcutManager, new IssueServiceWrapper(issueService),
                issueTableService, velocityRequestContextFactory,
                jqlStringSupport, preferencesManager, sessionSearchService, permissionManager, simpleLinkManager,
                defaultSearchUtil, featureManager, userSearchModeService, searcherService, searchRequestService,
                systemFilterService, avatarService, searchResultsHelper, beanBuilderFactory, webPanelMapperUtil,
                issueMetadataHelper, changeHistoryManager, actionUtilsService, webInterfaceManager, moduleWebComponent,
                userIssueHistoryManager, preferredSearchLayoutService, userPropertyManager, jiraPageBuilderService);
    }
}
