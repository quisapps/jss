package com.quisapps.jira.jss.action.quickedit;

import java.util.Map;
import java.util.Optional;

import com.atlassian.jira.bc.issue.IssueService;
import com.atlassian.jira.event.type.EventDispatchOption;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueInputParameters;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.workflow.TransitionOptions;

abstract class IssueServiceWrapperBase implements IssueService {
    
    protected IssueService issueService;
    
    IssueServiceWrapperBase(IssueService issueService) {
        this.issueService = issueService;
    }
    
    @Override
    public IssueInputParameters newIssueInputParameters() {
        return issueService.newIssueInputParameters();
    }
    
    @Override
    public IssueInputParameters newIssueInputParameters(
            Map<String, String[]> actionParameters)
    {
        return issueService.newIssueInputParameters(actionParameters);
    }
    
    @Override
    public IssueResult getIssue(ApplicationUser user, Long issueId) {
        return issueService.getIssue(user, issueId);
    }
    
    @Override
    public IssueResult getIssue(ApplicationUser user, String issueKey) {
        return issueService.getIssue(user, issueKey);
    }
    
    @Override
    public IssueResult assign(ApplicationUser user, AssignValidationResult assignResult) {
        return issueService.assign(user, assignResult);
    }
    
    @Override
    public IssueResult create(ApplicationUser user, CreateValidationResult createValidationResult,
                              String initialWorkflowActionName)
    {
        return issueService.create(user, createValidationResult, initialWorkflowActionName);
    }
    
    @Override
    public DeleteValidationResult validateDelete(ApplicationUser user, Long issueId) {
        return issueService.validateDelete(user, issueId);
    }
    
    @Override
    public ErrorCollection delete(ApplicationUser user, DeleteValidationResult deleteValidationResult) {
        return issueService.delete(user, deleteValidationResult);
    }
    
    @Override
    public ErrorCollection delete(ApplicationUser user, DeleteValidationResult deleteValidationResult,
                                  EventDispatchOption eventDispatchOption, boolean sendMail)
    {
        return issueService.delete(user, deleteValidationResult, eventDispatchOption, sendMail);
    }
    
    @Override
    public boolean isEditable(Issue issue, ApplicationUser user) {
        return issueService.isEditable(issue, user);
    }
    
    @Override
    public TransitionValidationResult validateTransition(ApplicationUser user, Long issueId, int actionId,
                                                         IssueInputParameters issueInputParameters)
    {
        return issueService.validateTransition(user, issueId, actionId, issueInputParameters);
    }
    
    @Override
    public TransitionValidationResult validateTransition(ApplicationUser user, Long issueId, int actionId,
                                                         IssueInputParameters issueInputParameters, TransitionOptions transitionOptions)
    {
        return issueService.validateTransition(user, issueId, actionId, issueInputParameters, transitionOptions);
    }
    
    @Override
    public IssueResult transition(ApplicationUser user, TransitionValidationResult transitionResult) {
        return issueService.transition(user, transitionResult);
    }
    
    @Override
    public AssignValidationResult validateAssign(ApplicationUser user, Long issueId, String assignee) {
        return issueService.validateAssign(user, issueId, assignee);
    }
    
    @Override
    public AsynchronousTaskResult clone(ApplicationUser arg0, CloneValidationResult arg1) {
        return issueService.clone(arg0, arg1);
    }
    
    @Override
    public CloneValidationResult validateClone(ApplicationUser arg0, Issue arg1, String arg2, boolean arg3,
                                               boolean arg4, boolean arg5, Map<CustomField, Optional<Boolean>> arg6)
    {
        return issueService.validateClone(arg0, arg1, arg2, arg3, arg4, arg5, arg6);
    }
}