package com.quisapps.jira.jss.action.quickedit;

import org.apache.log4j.Logger;

import com.atlassian.jira.bc.issue.IssueService;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.event.type.EventDispatchOption;
import com.atlassian.jira.issue.IssueInputParameters;
import com.atlassian.jira.user.ApplicationUser;
import com.quisapps.jira.jss.ScriptingManager;

public class IssueServiceWrapper extends IssueServiceWrapperBase {
    private static Logger log = Logger.getLogger(IssueServiceWrapper.class);
    
    private ScriptingManager scriptingManager;
    
    public IssueServiceWrapper(IssueService issueService) {
        super(issueService);
        
        this.scriptingManager = ComponentAccessor.getOSGiComponentInstanceOfType(ScriptingManager.class);
    }
    
    // UPDATE
    
    @Override
    public UpdateValidationResult validateUpdate(ApplicationUser user, Long issueId, IssueInputParameters issueInputParameters) {
        UpdateValidationResult validateUpdate =
                issueService.validateUpdate(user, issueId, issueInputParameters);
        
        try {
            scriptingManager.validateUpdate(
                    validateUpdate.getErrorCollection(),
                    validateUpdate.getIssue());
        } catch (Exception e) {
            log.warn(e.getMessage(), e);
        }
        
        return validateUpdate;
    }
    
    @Override
    public IssueResult update(ApplicationUser user, UpdateValidationResult updateValidationResult,
                              EventDispatchOption eventDispatchOption, boolean arg3)
    {
        try {
            scriptingManager.executeUpdatePostfunction(updateValidationResult.getIssue());
        } catch (Exception e) {
            log.warn(e.getMessage(), e);
        }
        
        return issueService.update(user, updateValidationResult, eventDispatchOption, arg3);
    }
    
    @Override
    public IssueResult update(ApplicationUser user, UpdateValidationResult updateValidationResult) {
        try {
            scriptingManager.executeUpdatePostfunction(updateValidationResult.getIssue());
        } catch (Exception e) {
            log.warn(e.getMessage(), e);
        }
        
        return issueService.update(user, updateValidationResult);
    }
    
    // CREATE
    
    @Override
    public CreateValidationResult validateCreate(ApplicationUser user, IssueInputParameters issueInputParameters) {
        CreateValidationResult result = issueService.validateCreate(user, issueInputParameters);
        
        try {
            scriptingManager.validateUpdate(result.getErrorCollection(), result.getIssue());
        } catch (Exception e) {
            log.warn(e.getMessage(), e);
        }
        
        return result;
    }
    
    @Override
    public CreateValidationResult validateSubTaskCreate(ApplicationUser user, Long parentId, IssueInputParameters issueInputParameters) {
        CreateValidationResult result = issueService.validateSubTaskCreate(user, parentId, issueInputParameters);
        
        try {
            scriptingManager.validateUpdate(result.getErrorCollection(), result.getIssue());
        } catch (Exception e) {
            log.warn(e.getMessage(), e);
        }
        
        return result;
    }
    
    @Override
    public IssueResult create(ApplicationUser user, CreateValidationResult createValidationResult, String initialWorkflowActionName) {
        try {
            scriptingManager.executeUpdatePostfunction(createValidationResult.getIssue());
        } catch (Exception e) {
            log.warn(e.getMessage(), e);
        }
        
        return issueService.create(user, createValidationResult, initialWorkflowActionName);
    }
    
    @Override
    public IssueResult create(ApplicationUser user, CreateValidationResult createValidationResult) {
        try {
            scriptingManager.executeUpdatePostfunction(createValidationResult.getIssue());
        } catch (Exception e) {
            log.warn(e.getMessage(), e);
        }
        
        return issueService.create(user, createValidationResult);
    }
}
