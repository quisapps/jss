package com.quisapps.jira.jss.action.admin;

import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.request.RequestMethod;
import com.atlassian.jira.security.request.SupportedMethods;
import com.quisapps.jira.jss.ScriptingManager;
import com.quisapps.jira.jss.install.Installer;

public abstract class JSSGlobalScriptEditorBase extends JSSSysScriptEditor {
    
    private Installer installer;
    
    public JSSGlobalScriptEditorBase(PermissionManager permissionManager,
                                     ApplicationProperties applicationProperties,
                                     ScriptingManager scriptingManager, Installer installer)
    {
        super(permissionManager, applicationProperties, scriptingManager);
        
        this.installer = installer;
    }
    
    @Override
    @SupportedMethods({RequestMethod.GET, RequestMethod.POST})
    protected String doExecute() throws Exception {
        if (!installer.isQuickEditEnabled()) {
            addErrorMessage("<p>Global Edit Validation & Postfunction scripts are disabled due" +
                    " to installation problem: incompatible Quick Edit plugin.</p>" +
                    "<p>Want this out of the box? <a href='https://marketplace.atlassian.com/1216350'>Consider going Pro!</a></p>");
        }
        
        return super.doExecute();
    }
}
