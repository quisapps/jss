package com.quisapps.jira.jss.action;

import com.atlassian.jira.bc.issue.IssueService;
import com.atlassian.jira.bc.issue.comment.CommentService;
import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.config.SubTaskManager;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutManager;
import com.atlassian.jira.issue.fields.screen.FieldScreenRendererFactory;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.user.util.UserUtil;
import com.atlassian.jira.workflow.WorkflowManager;
import com.quisapps.jira.jss.ScriptingManager;

@SuppressWarnings("serial")
public class EditIssue extends com.atlassian.jira.web.action.issue.EditIssue {
    
    private ScriptingManager scriptingManager;
    
    public EditIssue(SubTaskManager subTaskManager,
                     ConstantsManager constantsManager,
                     FieldLayoutManager fieldLayoutManager,
                     WorkflowManager workflowManager,
                     FieldScreenRendererFactory fieldScreenRendererFactory,
                     CommentService commentService,
                     IssueService issueService,
                     UserUtil userUtil,
                     UserManager userManager,
                     ScriptingManager scriptingManager)
    {
        super(subTaskManager, constantsManager, fieldLayoutManager, workflowManager,
                fieldScreenRendererFactory, commentService, issueService, userUtil, userManager);
        this.scriptingManager = scriptingManager;
    }
    
    @Override
    protected void doValidation() {
        super.doValidation();
        
        try {
            scriptingManager.validateUpdate(this, getMutableIssue());
        } catch (Exception ex) {
            addErrorMessage(ex.getMessage());
        }
    }
    
    @Override
    protected String doExecute() throws Exception
    {
        scriptingManager.executeUpdatePostfunction(getMutableIssue());
        return super.doExecute();
    }
}
