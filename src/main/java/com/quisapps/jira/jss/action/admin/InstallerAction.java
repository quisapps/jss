package com.quisapps.jira.jss.action.admin;

import com.atlassian.jira.security.request.RequestMethod;
import com.atlassian.jira.security.request.SupportedMethods;
import com.atlassian.jira.web.action.JiraWebActionSupport;

@SupportedMethods({RequestMethod.GET, RequestMethod.POST})
public class InstallerAction extends JiraWebActionSupport {
    private static final long serialVersionUID = 6845811484021820235L;
}
