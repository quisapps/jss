package com.quisapps.jira.jss.action.admin;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.security.request.RequestMethod;
import com.atlassian.jira.security.request.SupportedMethods;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.quisapps.jira.jss.ScriptingManager;
import com.quisapps.jira.jss.jython.JythonUtil;

public abstract class JSSSysScriptEditor extends JiraWebActionSupport {
    
    public static final String JYTHON_SOURCE_TYPE_INLINE = "inline";
    public static final String JYTHON_SOURCE_TYPE_FILE = "FILE";
    
    protected PermissionManager permissionManager;
    protected ApplicationProperties applicationProperties;
    protected ScriptingManager scriptingManager;
    
    private String jythonSource;
    private boolean submitted;
    private boolean reload;
    private String jythonSourceType = JYTHON_SOURCE_TYPE_FILE;
    
    public JSSSysScriptEditor(PermissionManager permissionManager,
                              ApplicationProperties applicationProperties,
                              ScriptingManager scriptingManager)
    {
        
        this.permissionManager = permissionManager;
        this.applicationProperties = applicationProperties;
        this.scriptingManager = scriptingManager;
    }
    
    public abstract String getSourceTypeProperty();
    
    public abstract String getSourceProperty();
    
    public abstract String getDefaultFileName();
    
    @Override
    @SupportedMethods({RequestMethod.GET, RequestMethod.POST})
    protected String doExecute() throws Exception {
        if (!permissionManager.hasPermission(Permissions.ADMINISTER, getLoggedInApplicationUser())) {
            return LOGIN; //getRedirect("Dashboard.jspa");
        }
        
        String propSourceType = getSourceTypeProperty();
        String propSource = getSourceProperty();
        
        if (isReload()) {
            applicationProperties.setString(propSourceType, getJythonSourceType());
            setSubmitted(false);
            // isSubmitted() should be false here!
        }
        
        //check page has been submitted
        if (!isSubmitted()) {
            setJythonSourceType(applicationProperties.getString(propSourceType));
            if (JYTHON_SOURCE_TYPE_INLINE.equals(getJythonSourceType())) {
                setJythonSource(applicationProperties.getText(propSource));
            } else {
                setJythonSource(loadJythonSourceFile());
            }
            return INPUT;
        }
        
        applicationProperties.setString(propSourceType, getJythonSourceType());
        if (JYTHON_SOURCE_TYPE_INLINE.equals(getJythonSourceType())) {
            applicationProperties.setText(propSource, getJythonSource());
        } else {
            saveJythonSourceFile(getJythonSource());
        }
        
        return INPUT;
    }
    
    protected String loadJythonSourceFile() {
        StringBuffer contents = new StringBuffer();
        
        String separator = System.getProperty("line.separator");
        try (Scanner s = new Scanner(getJythonSourceFile())) {
            while (s.hasNextLine()) {
                contents.append(s.nextLine()).append(separator);
            }
        } catch (FileNotFoundException e) {
            return e.getMessage();
        }
        
        return contents.toString();
    }
    
    protected void saveJythonSourceFile(String content) throws IOException {
        BufferedWriter w = new BufferedWriter(new FileWriter(getJythonSourceFile()));
        w.write(content);
        w.close();
    }
    
    public File getJythonSourceFile() {
        return new File(new File(JythonUtil.getJythonPath(), "sys"), getDefaultFileName());
    }
    
    public void setJythonSource(String jythonSource) {
        this.jythonSource = jythonSource;
    }
    
    public String getJythonSource() {
        return jythonSource;
    }
    
    public void setSubmitted(boolean submitted) {
        this.submitted = submitted;
    }
    
    public boolean isSubmitted() {
        return submitted;
    }
    
    public void setJythonSourceType(String jythonSourceType) {
        if (null == jythonSourceType) {
            this.jythonSourceType = JYTHON_SOURCE_TYPE_FILE;
        } else {
            this.jythonSourceType = jythonSourceType;
        }
    }
    
    public String getJythonSourceType() {
        return jythonSourceType;
    }
    
    public void setReload(boolean reload) {
        this.reload = reload;
    }
    
    public boolean isReload() {
        return reload;
    }
}
