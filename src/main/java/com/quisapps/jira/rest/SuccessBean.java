/**
 *
 */
package com.quisapps.jira.rest;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class SuccessBean {
    
    private Object result;
    
    public SuccessBean() {}
    
    public SuccessBean(Object r) {
        this.result = r;
    }
    
    public void setResult(Object result) {
        this.result = result;
    }
    
    public Object getResult() {
        return result;
    }
}