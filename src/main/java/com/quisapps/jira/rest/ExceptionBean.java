/**
 *
 */
package com.quisapps.jira.rest;

import javax.xml.bind.annotation.XmlRootElement;

import org.python.core.PyException;

@XmlRootElement
public class ExceptionBean {
    
    private String message;
    private String exClass;
    
    public ExceptionBean() {
        this.setMessage("");
        this.setExClass("");
    }
    
    public ExceptionBean(Throwable t) {
        if (t instanceof PyException) {
            PyException ex = (PyException) t;
            message = ex.value.toString();
            exClass = ex.type.toString();
        } else {
            this.setMessage(t.getMessage());
            this.setExClass(t.getClass().getName());
        }
        
        if (this.getMessage() == null) {
            this.setMessage("No message");
        }
    }
    
    public void setMessage(String message) {
        this.message = message;
    }
    
    public String getMessage() {
        return message;
    }
    
    public void setExClass(String exClass) {
        this.exClass = exClass;
    }
    
    public String getExClass() {
        return exClass;
    }
}