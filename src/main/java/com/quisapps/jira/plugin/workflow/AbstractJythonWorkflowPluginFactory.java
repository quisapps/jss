package com.quisapps.jira.plugin.workflow;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.plugin.JiraResourcedModuleDescriptor;
import com.atlassian.jira.plugin.workflow.AbstractWorkflowPluginFactory;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.opensymphony.workflow.loader.AbstractDescriptor;
import com.opensymphony.workflow.loader.ConditionDescriptor;
import com.opensymphony.workflow.loader.FunctionDescriptor;
import com.opensymphony.workflow.loader.ValidatorDescriptor;

@SuppressWarnings({ "unchecked", "rawtypes" })
public abstract class AbstractJythonWorkflowPluginFactory extends
        AbstractWorkflowPluginFactory {
    
    public static final String SOURCE_TYPE_FILE = "file";
    public static final String SOURCE_TYPE_INLINE = "inline";
    public static final String SOURCE_TYPE_FIELD_NAME = "jythonSourceType";
    public static final String FILE_SELECT_FIELD_NAME = "jythonFileSelect";
    public static final String NEW_FILENAME_FIELD_NAME = "jythonNewFileName";
    public static final String SCRIPT_FIELD_NAME = "jythonSource";
    
    protected String MODULE_NAME = "";
    
    @Override
    protected void getVelocityParamsForEdit(Map velocityParams, AbstractDescriptor descriptor) {
        Map args = getDescriptorArgs(descriptor);
        velocityParams.putAll(args);
        getVelocityParamsForInput(velocityParams);
    }
    
    @Override
    protected void getVelocityParamsForInput(Map velocityParams) {
        CustomFieldManager cfm = ComponentAccessor.getCustomFieldManager();
        velocityParams.put("list-customFields", cfm.getCustomFieldObjects());
        velocityParams.put("jythonFileUtil", new JythonFileUtil());
        
        PluginAccessor pm = ComponentAccessor.getPluginAccessor();
        Plugin p = pm.getPlugin("com.quisapps.jira.jss");
        JiraResourcedModuleDescriptor d =
                (JiraResourcedModuleDescriptor) p.getModuleDescriptor(MODULE_NAME);
        velocityParams.put("includeJythonFilePanel",
                d.getHtml("jython-file-panel", velocityParams));
        velocityParams.put("includeJythonReference",
                d.getHtml("jython-reference", velocityParams));
    }
    
    @Override
    protected void getVelocityParamsForView(Map velocityParams, AbstractDescriptor descriptor) {
        velocityParams.put("TUID", new Date().getTime());
        
        Map args = getDescriptorArgs(descriptor);
        velocityParams.putAll(args);
        
        String condition = args.get(SCRIPT_FIELD_NAME).toString();
        if (condition != null && condition.startsWith("@")) {
            velocityParams.put(FILE_SELECT_FIELD_NAME, condition.substring(1));
            velocityParams.put(SCRIPT_FIELD_NAME,
                    JythonFileUtil.getInstance().getFileContent(condition.substring(1)));
        }
    }
    
    private Map getDescriptorArgs(AbstractDescriptor descriptor) {
        Map args = new HashMap();
        if (descriptor instanceof ValidatorDescriptor)
            args = ((ValidatorDescriptor) descriptor).getArgs();
        else if (descriptor instanceof ConditionDescriptor)
            args = ((ConditionDescriptor) descriptor).getArgs();
        else if (descriptor instanceof FunctionDescriptor)
            args = ((FunctionDescriptor) descriptor).getArgs();
        return args;
    }
    
    @Override
    public Map getDescriptorParams(Map formParams) {
        Map m = new HashMap();
        
        String jythonSourceType = getStringParam(formParams, SOURCE_TYPE_FIELD_NAME);
        if (null == jythonSourceType || SOURCE_TYPE_INLINE.equals(jythonSourceType)) {
            m.put(SCRIPT_FIELD_NAME, getStringParam(formParams, SCRIPT_FIELD_NAME));
        } else if (SOURCE_TYPE_FILE.equals(jythonSourceType)) {
            String fileName = getStringParam(formParams, FILE_SELECT_FIELD_NAME);
            if ("-1".equals(fileName)) {
                fileName = getStringParam(formParams, NEW_FILENAME_FIELD_NAME);
            }
            m.put(SCRIPT_FIELD_NAME, "@" + fileName);
            String content = getStringParam(formParams, SCRIPT_FIELD_NAME);
            try {
                JythonFileUtil.getInstance().saveFileContent(fileName, content);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        
        return m;
    }
    
    private String getStringParam(Map formParams, String paramName) {
        String[] c = (String[]) formParams.get(paramName);
        if (c != null && c.length > 0)
            return c[0];
        else
            return "";
    }
}
