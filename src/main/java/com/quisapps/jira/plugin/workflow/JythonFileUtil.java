package com.quisapps.jira.plugin.workflow;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Matcher;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.quisapps.jira.jss.jython.JythonUtil;

@Path("/jython/files")
public class JythonFileUtil {
    
    private static JythonFileUtil instance = null;
    
    public static JythonFileUtil getInstance() {
        if (null == instance)
            instance = new JythonFileUtil();
        return instance;
    }
    
    public List<String> getFiles() {
        String jythonPath = getJythonPath();
        
        String pattern = ("" + jythonPath + File.separatorChar);
        pattern = Matcher.quoteReplacement(pattern).replace("(", "\\(").replace(")", "\\)");
        
        Set<String> retval = new TreeSet<String>();
        List<File> dirs2list = new ArrayList<File>();
        dirs2list.add(new File(jythonPath));
        while (dirs2list.size() > 0) {
            List<File> newList = new ArrayList<File>();
            for (File d : dirs2list) {
                for (File f : d.listFiles()) {
                    if (f.isDirectory())
                        newList.add(f);
                    else if (!f.getName().equals(JythonUtil.__INIT_INTERPRETER___PY)) {
                        String path = f.getPath();
                        path = path.replaceAll(pattern, "");
                        retval.add(path);
                    }
                }
            }
            dirs2list = newList;
        }
        
        return Arrays.asList(retval.toArray(new String[] {}));
    }
    
    @GET
    @Path("{fileName:.+}")
    @AnonymousAllowed
    @Produces({ MediaType.TEXT_PLAIN })
    public String getFileContent(@PathParam("fileName") String fileName) {
        ApplicationUser user = ComponentAccessor.getJiraAuthenticationContext().getUser();
        PermissionManager permissionManager = ComponentAccessor.getPermissionManager();
        if (!permissionManager.hasPermission(Permissions.ADMINISTER, user)) {
            return "You are not authorized to view Jython scripts";
        }
        
        String jythonPath = getJythonPath();
        StringBuffer contents = new StringBuffer();
        
        String separator = System.getProperty("line.separator");
        try (Scanner s = new Scanner(new File(jythonPath, fileName))) {
            while (s.hasNextLine()) {
                contents.append(s.nextLine()).append(separator);
            }
        } catch (FileNotFoundException e) {
            return e.getMessage();
        }
        
        return contents.toString();
    }
    
    public void saveFileContent(String fileName, String content) throws IOException {
        String jythonPath = getJythonPath();
        BufferedWriter w = new BufferedWriter(new FileWriter(new File(jythonPath, fileName)));
        w.write(content);
        w.close();
    }
    
    public String getJythonPath() {
        File p = new File(JythonUtil.getJythonPath(), "workflow");
        if (!p.exists()) {
            p.mkdirs();
        }
        
        return p.getAbsolutePath();
    }
}
