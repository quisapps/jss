package com.quisapps.jira.plugin.workflow;

import com.atlassian.jira.plugin.workflow.WorkflowPluginConditionFactory;

public class JythonConditionFactory
        extends AbstractJythonWorkflowPluginFactory
        implements WorkflowPluginConditionFactory {
    
    {
        MODULE_NAME = "quisapps.jython.condition";
    }
}
