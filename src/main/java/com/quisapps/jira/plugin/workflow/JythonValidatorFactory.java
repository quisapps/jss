package com.quisapps.jira.plugin.workflow;

import com.atlassian.jira.plugin.workflow.WorkflowPluginValidatorFactory;

public class JythonValidatorFactory extends
        AbstractJythonWorkflowPluginFactory implements WorkflowPluginValidatorFactory {
    
    {
        MODULE_NAME = "quisapps.jython.validator";
    }
}
