from java.io import ByteArrayOutputStream, PrintWriter
from com.atlassian.jira.component import ComponentAccessor
from com.atlassian.jira.security import Permissions
from com.quisapps.jira.jss.jython import JythonUtil, JythonContext
from org.apache.log4j import Logger, Level
from java.lang import System

def checkUser():
    authenticationContext = ComponentAccessor.getJiraAuthenticationContext()
    user = authenticationContext.getUser()

    if not user:
        return None

    pm = ComponentAccessor.getPermissionManager()
    if not pm.hasPermission(Permissions.ADMINISTER, user):
        return None

    return user

###########################################################
def run(args):
    try:
        if not checkUser():
            return {"err" : "You are not authorized to use Jython Runner"}
    except Exception, e:
        return {"err" : e.__str__()}

    try:
        script = args['script']
    except:
        return {"err" : "Invalid input"}

    err = ByteArrayOutputStream()
    out = ByteArrayOutputStream()

    pi = JythonUtil.getNewInterpreter()
    pi.setErr(err)
    pi.setOut(out)

    try:
        log = Logger.getLogger(__name__)
        log.setLevel(Level.DEBUG)
        pi.set(JythonContext.LOG, log)

        pi.exec(script)
    except Exception, e:
        p = PrintWriter(err)
        p.println(e.__str__())
        p.flush()

    pi.setErr(System.err)
    pi.setOut(System.out)

    return {
        "out" : out.toString(),
        "err" : err.toString()
        }