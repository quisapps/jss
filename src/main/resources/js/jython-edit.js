var jythonTextFieldChanged = false;
var jythonTextFieldName = "jythonSource";
var jythonFileSelectPrevValues = null;

function jythonInit(fieldName) {
    jythonTextFieldName = fieldName;

    jythonFileSelectPrevValues = jQuery('#jythonFileSelect').val();

    var dialogDiscard = AJS.dialog2('#dialog-discard');

    jQuery('#dialog-discard-yes').click(function(e) {
        dialogDiscard.hide();
        loadJython(jQuery('#jythonFileSelect').val());
    });

    jQuery('#dialog-discard-no').click(function(e) {
        dialogDiscard.hide();
    });

    jQuery('#dialog-discard-cancel').click(function(e) {
        jQuery('#jythonFileSelect').val(jythonFileSelectPrevValues);
        dialogDiscard.hide();
    });

    jQuery('#jythonFileSelect').change(function() {
        jQuery('#jythonNewFileName').attr('disabled', (this.value != '-1'));
        if (this.value != '-1') {
            if (jythonTextFieldChanged) {
                dialogDiscard.show();
            } else {
                jythonFileSelectPrevValues = jQuery('#jythonFileSelect').val();
                loadJython(this.value);
            }
        }
    });
    jQuery('#jythonSourceTypeInline').click(function() {
        jQuery('#jythonFileSelect,#jythonNewFileName').attr('disabled', true);
    });
    jQuery('#jythonSourceTypeFile').click(function() {
        jQuery('#jythonFileSelect').attr('disabled', false).trigger('change');
    });

    jQuery('#jythonDefaults').hide();
    jQuery('#jythonDefaultsToggle').click(function() { jQuery('#jythonDefaults').slideToggle('slow') });

    editAreaLoader.init({
        id : jythonTextFieldName    // textarea id
        ,syntax: "python"           // syntax to be uses for highgliting
        ,start_highlight: true      // to display with highlight mode on start-up
        ,change_callback: "valChanged"
    });

    var text = jQuery('#'+jythonTextFieldName).val();
    if (text.charAt(0) == '@') {
        jQuery('#jythonFileSelect option[value="' + text.substring(1) + '"]').attr('selected', 1);
        jQuery('#jythonSourceTypeFile').attr('checked', true);
        jQuery('#jythonFileSelect').attr('disabled', false).trigger('change');
    }
}

function loadJython(file) {
    var timeoutId = setTimeout(function() {
        AJS.dim();
        JIRA.Loading.showLoadingIndicator();
    }, 500);

    var stopLoading = function() {
        clearTimeout(timeoutId);
        if (JIRA.Loading.isVisible()) {
            JIRA.Loading.hideLoadingIndicator();
            AJS.undim();
        }
    }

    jQuery.ajax({
        url: contextPath + '/rest/jss/1.0/jython/files/' + file,
        type: 'GET',
        success: function(data) {
            stopLoading();
            editAreaLoader.setValue(jythonTextFieldName, data);
            jythonTextFieldChanged = false;
            editAreaLoader.setSelectionRange(jythonTextFieldName, 0, 0);
        },
        complete: function() {
            stopLoading();
        }
    });
}

function valChanged(id) {
    jythonTextFieldChanged = true;
}

