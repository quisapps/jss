#!/bin/bash

set -e

cp ./target/jira/webapp/WEB-INF/classes/log4jdev.properties ./target/jira/webapp/WEB-INF/classes/log4j.properties
cp ./target/container/tomcat8x/cargo-jira-home/webapps/jira/WEB-INF/classes/log4jdev.properties  ./target/container/tomcat8x/cargo-jira-home/webapps/jira/WEB-INF/classes/log4j.properties

echo OK
